Build (to run withour docker follow http://wiki.ros.org/melodic/Installation/Ubuntu and skip to p.3):

1. docker pull tgos/robot_fpv:0.1.1

2. sudo docker run -it tgos/robot_fpv:0.1.1 /bin/bash

3. sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

4. apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

5. apt update

6. mkdir -p catkin_ws/src

7. cd catkin_ws

8. git -C src clone https://gitlab.com/otoshi/xberry_sensor_network

9. rosdep update 

10. rosdep install -y --from-paths src/xberry_sensor_network --ignore-src

11. catkin_make

Run (you should be in catkin_ws dir):

1. source devel/setup.bash

2. roslaunch xberry_sensor_network sensor_network.launch &

3. rosrun xberry_sensor_network consumer

To use dynamic reconfigure:

1. apt install ros-melodic-rqt-reconfigure

2. rosrun rqt_reconfigure rqt_reconfigure

rqt_reconfigure is a GUI app, so to use it inside docker you need to enable X forwarding.