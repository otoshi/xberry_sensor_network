#include <ros/ros.h>
#include <variant>

#pragma once

class Sensor
{
  public:
    virtual ~Sensor();

    virtual void publish() = 0;

  protected:
    Sensor();

    virtual std::variant<void*, int32_t, double> getData() = 0;

    std::variant<void*, int32_t, double> data_;

    ros::NodeHandle nh_;

  private:
    Sensor(const Sensor&)= delete;
    Sensor& operator=(const Sensor&)= delete;
};

class TemperatureSensor : public Sensor
{
  public:
    explicit TemperatureSensor();
    ~TemperatureSensor();

    virtual void publish() override;

  protected:
    std::variant<void*, int32_t, double> getData() override;
    
    ros::Publisher publisher_;

};

class PressureSensor : public Sensor
{
  public:
    explicit PressureSensor();
    ~PressureSensor();

    virtual void publish() override;

  protected:
    std::variant<void*, int32_t, double> getData() override;

    ros::Publisher publisher_;

};

