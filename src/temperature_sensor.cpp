#include <ros/ros.h>
#include <std_msgs/Float64.h>

#include <dynamic_reconfigure/server.h>
#include <xberry_sensor_network/sensorConfig.h>

#include "Sensor.h"

static double pubRate = 2;

void callback(xberry_sensor_network::sensorConfig &config, uint32_t level)
{
  ROS_INFO_STREAM("Pub rate changed to: " << config.pubRate << "\n");
  pubRate = config.pubRate;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "temperature_sensor");

  dynamic_reconfigure::Server<xberry_sensor_network::sensorConfig> server;
  dynamic_reconfigure::Server<xberry_sensor_network::sensorConfig>::CallbackType f;

  f = boost::bind(&callback, _1, _2);
  server.setCallback(f);

  TemperatureSensor s;

  while (ros::ok())
  {
    s.publish();

    ros::spinOnce();

    ros::Duration(1/pubRate).sleep();
  }
  
  return 0;
}