#pragma once

#include <random>

class DataGenerator
{
  public:
    static DataGenerator& getInstance()
    {
      static DataGenerator instance;
      return instance;
    }

    double getSequentialDouble()
    {
      return sequentialDoubleCounter++;
    }

    double getRandomDouble()
    {
      return dist_(mt_);
    }

  private:
    DataGenerator()
    : mt_(rd_()), dist_(1.0, 10.0)
    {}
    ~DataGenerator()= default;
    DataGenerator(const DataGenerator&)= delete;
    DataGenerator& operator=(const DataGenerator&)= delete;
  
  double sequentialDoubleCounter = 0.0;

  //random generator
  std::random_device rd_;
  std::mt19937 mt_;
  std::uniform_real_distribution<double> dist_;
};
