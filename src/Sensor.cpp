#include "Sensor.h"
#include <xberry_sensor_network/float_sensor_data.h>
#include "DataGenerator.h"

Sensor::Sensor()
{
}

Sensor::~Sensor()
{
}

TemperatureSensor::TemperatureSensor()
: publisher_(nh_.advertise<xberry_sensor_network::float_sensor_data>("sensor_data/temperature", 10))
{
}

TemperatureSensor::~TemperatureSensor()
{
}

std::variant<void*, int32_t, double> TemperatureSensor::getData()
{
  return DataGenerator::getInstance().getSequentialDouble();
}

void TemperatureSensor::publish()
{
  xberry_sensor_network::float_sensor_data msg;

  try 
  {
    msg.sensor_name = ros::this_node::getName();
    msg.sensor_reading = std::get<double>(getData());
  }
  catch (const std::bad_variant_access&) 
  {
    ROS_ERROR_STREAM("Sensor read value has wrong type!");
    return;
  }

  ROS_INFO_STREAM("Got reading from temperature sensor " << msg.sensor_name << ". Temperature is: " << msg.sensor_reading);

  publisher_.publish(msg);

  return;
}

PressureSensor::PressureSensor()
: publisher_(nh_.advertise<xberry_sensor_network::float_sensor_data>("sensor_data/pressure", 10))
{
}

PressureSensor::~PressureSensor()
{
}

std::variant<void*, int32_t, double> PressureSensor::getData()
{
  return DataGenerator::getInstance().getRandomDouble();
}

void PressureSensor::publish()
{
  xberry_sensor_network::float_sensor_data msg;

  try 
  {
    msg.sensor_name = ros::this_node::getName();
    msg.sensor_reading = std::get<double>(getData());
  }
  catch (const std::bad_variant_access&) 
  {
    ROS_ERROR_STREAM("Sensor read value has wrong type!");
    return;
  }

  ROS_INFO_STREAM("Got reading from temperature sensor " << msg.sensor_name << ". Temperature is: " << msg.sensor_reading);

  publisher_.publish(msg);

  return;
}