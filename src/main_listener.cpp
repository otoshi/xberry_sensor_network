#include "ros/ros.h"
#include "Listener.h"
#include "std_msgs/String.h"
#include <thread>

constexpr int DEFAULT_BUFFER_SIZE = 512;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");

  RingBuffer<std::pair<std::string, double>> buffer(DEFAULT_BUFFER_SIZE);

  Listener<const xberry_sensor_network::float_sensor_data> temperatureListener("sensor_data/temperature", buffer);
  Listener<const xberry_sensor_network::float_sensor_data> pressureListener("sensor_data/pressure", buffer);

  ros::NodeHandle n;
  ros::Publisher resultsPub = n.advertise<std_msgs::String>("acc_results", 1000);

  ros::Rate loop_rate(1);

  std_msgs::String msg;

  while (ros::ok())
  {
    msg.data = buffer.dump();

    ROS_INFO_STREAM("Buffer dump: " << msg.data);

    resultsPub.publish(msg);

    buffer.reset();

    ros::spinOnce();

    loop_rate.sleep();
  }

  ros::spin();

  return 0;
}