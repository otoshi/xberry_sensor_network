#pragma once

// from https://embeddedartistry.com/blog/2017/05/17/creating-a-circular-buffer-in-c-and-c/

#include <memory>
#include <mutex>
#include <sstream>

template <class T>
class RingBuffer {
public:
	explicit RingBuffer(size_t size) :
		buf_(std::unique_ptr<T[]>(new T[size])),
		max_size_(size)
	{}

	void put(T item);
	T get();
	void reset();
	bool empty() const;
	bool full() const;
	size_t capacity() const;
	size_t size() const;
  std::string dump() const;

private:
  std::mutex mutex_;
	std::unique_ptr<T[]> buf_;
	size_t head_ = 0;
	size_t tail_ = 0;
	const size_t max_size_;
	bool full_ = 0;
};

template <typename T>
void RingBuffer<T>::reset()
{
  std::lock_guard<std::mutex> lock(mutex_);

	head_ = tail_;
	full_ = false;
}

template <typename T>
bool RingBuffer<T>::empty() const
{
	//if head and tail are equal, we are empty
	return (!full_ && (head_ == tail_));
}

template <typename T>
bool RingBuffer<T>::full() const
{
	//If tail is ahead the head by 1, we are full
	return full_;
}

template <typename T>
size_t RingBuffer<T>::capacity() const
{
	return max_size_;
}

template <typename T>
size_t RingBuffer<T>::size() const
{
	size_t size = max_size_;

	if(!full_)
	{
		if(head_ >= tail_)
		{
			size = head_ - tail_;
		}
		else
		{
			size = max_size_ + head_ - tail_;
		}
	}

	return size;
}

template <typename T>
void RingBuffer<T>::put(T item)
{
  std::lock_guard<std::mutex> lock(mutex_);

	buf_[head_] = item;

	if(full_)
	{
		tail_ = (tail_ + 1) % max_size_;
	}

	head_ = (head_ + 1) % max_size_;

	full_ = head_ == tail_;
}

template <typename T>
T RingBuffer<T>::get()
{
  std::lock_guard<std::mutex> lock(mutex_);

	if(empty())
	{
		return T();
	}

	//Read data and advance the tail (we now have a free space)
	auto val = buf_[tail_];
	full_ = false;
	tail_ = (tail_ + 1) % max_size_;

	return val;
}

template <typename T>
std::string RingBuffer<T>::dump() const
{
  std::stringstream ss;

  ss << size() << " ";

  for (size_t i = 0; i < size(); i++)
  {
    ss << buf_[i].first << " " << buf_[i].second << "\n";
  }
  
  return ss.str();
}
