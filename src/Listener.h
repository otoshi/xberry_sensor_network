#pragma once

#include <ros/ros.h>
#include <xberry_sensor_network/float_sensor_data.h>
#include <xberry_sensor_network/bool_sensor_data.h>

#include "RingBuffer.h"
#include <utility>

template <typename T>
class Listener
{
  public:
    void callback(T& msg);

    explicit Listener(const char* topic, RingBuffer<std::pair<std::string, double>>& buffer);
    virtual ~Listener();

  protected:

    ros::NodeHandle nh_;
    ros::Subscriber sub_;
    RingBuffer<std::pair<std::string, double>>& buffer_;
};

template <typename T>
Listener<T>::Listener(const char* topic, RingBuffer<std::pair<std::string, double>>& buffer)
: buffer_(buffer)
{
  sub_ = nh_.subscribe(topic, 10, &Listener<T>::callback, this);
}

template <typename T> 
Listener<T>::~Listener()
{
}

template <typename T> 
void Listener<T>::callback(T& msg)
{
  std::pair<std::string, double>r;
  r.first = msg.sensor_name;
  r.second = msg.sensor_reading;
  ROS_INFO_STREAM("I heard: " << msg.sensor_reading << " from " << msg.sensor_name);
  buffer_.put(r);
}